FROM golang:1.19.0-alpine3.15
WORKDIR /carp-metrics-collection-plugin
RUN apk update && apk upgrade
RUN apk add --no-cache gcc musl-dev 
COPY . .
RUN go build -o main ./cmd/main/main.go
CMD ["./main"]
