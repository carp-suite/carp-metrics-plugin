package internal_test

import (
	"context"
	"log"
	"net"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/helpers"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/server"
	"gitlab.com/carp-suite/plugins/carpdriver"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

type pluginTest struct {
	mt         *mockMetricTime
	plugin     *core.MetricsCollectionPlugin
	controller *server.MetricsCollectionPluginController
	listener   *bufconn.Listener
	ctx        context.Context
	client     pb.PluginClient
	grpcServer *grpc.Server
	result     interface{}
	results    map[string]interface{}
	mu         sync.Mutex
}

var methods map[string]int16 = map[string]int16{
	"get":     1,
	"post":    2,
	"patch":   3,
	"put":     4,
	"delete":  5,
	"options": 6,
}

type mockMetricTime struct {
	microseconds int
}

func (mct mockMetricTime) Now() time.Time {
	t := time.Date(0, 0, 0, 0, 0, 0, mct.microseconds*1000, time.UTC)
	return t
}

func (mct mockMetricTime) Since(t time.Time) time.Duration {
	return mct.Now().Sub(t)
}

func TestMetricCollectionPluginService(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./plugin.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	st := new(pluginTest)

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := st.setup(sc)
		return ctx, err
	})

	sc.Step(`^the request start and end:$`, st.requestStartAndEnd)
	sc.Step(`^the cache end:$`, st.cacheEnd)
	sc.Step(`^now is the (\d+) microsecond$`, st.now)
	sc.Step(`^I get (.+)$`, st.get)
	sc.Step(`^the (.+) should match json$`, st.itShouldMatchJson)
}

func (st *pluginTest) setup(sc *godog.Scenario) error {
	st.results = make(map[string]interface{})
	st.mt = new(mockMetricTime)
	st.plugin = core.NewMetricsCollectionPlugin(st.mt)
	st.controller = &server.MetricsCollectionPluginController{
		Plugin: st.plugin,
	}

	bufSize := 1024 * 1024
	st.listener = bufconn.Listen(bufSize)
	gs := carpdriver.InitGrpc(st.plugin)
	go func() {
		if err := gs.Serve(st.listener); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()

	if st.client != nil {
		return nil
	}

	st.ctx = context.Background()
	conn, err := grpc.DialContext(st.ctx, "bufnet", grpc.WithContextDialer(st.dialer), grpc.WithInsecure())
	if err != nil {
		return err
	}

	st.client = pb.NewPluginClient(conn)

	return nil
}

func (st *pluginTest) dialer(context.Context, string) (net.Conn, error) {
	return st.listener.Dial()
}

func (st *pluginTest) requestStartAndEnd(requests *godog.Table) error {
	type request struct {
		Id     string
		Path   string
		Method string
		Start  int
		Status int
		End    int
	}

	assist := assistdog.NewDefault()
	result, err := assist.CreateSlice(new(request), requests)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	results := result.([]*request)

	for _, r := range results {
		streamStart, err := st.client.RequestStart(st.ctx)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		id, _ := uuid.Parse(r.Id)
		request := &pb.HttpRequest{
			Id:     id[:],
			Path:   r.Path,
			Method: pb.Method(methods[r.Method]),
		}

		st.mt.microseconds = r.Start
		if err := streamStart.Send(request); err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		streamStart.CloseSend()

		_, err = streamStart.Recv()
		if err != nil {
			return (helpers.AssertActual(assert.NoError, err, err.Error()))
		}

		streamEnd, err := st.client.RequestEnd(st.ctx)
		if err != nil {
			return (helpers.AssertActual(assert.NoError, err, err.Error()))
		}

		id, _ = uuid.Parse(r.Id)
		response := &pb.HttpResponse{
			Id:     id[:],
			Status: int32(r.Status),
		}

		st.mt.microseconds = r.End
		if err := streamEnd.Send(response); err != nil {
			return (helpers.AssertActual(assert.NoError, err, err.Error()))
		}

		_, err = streamEnd.Recv()
		if err != nil {
			return (helpers.AssertActual(assert.NoError, err, err.Error()))
		}

		streamEnd.CloseSend()

	}

	return nil
}

func (st *pluginTest) cacheEnd(table *godog.Table) error {
	type cache struct {
		Id        string
		BytesSize int
	}

	assist := assistdog.NewDefault()
	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	caches := result.([]*cache)

	for _, r := range caches {
		streamCacheEnd, err := st.client.CacheEnd(st.ctx)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		id, _ := uuid.Parse(r.Id)
		cacheRes := &pb.CacheResponse{
			Id:           id[:],
			ResponseSize: float64(r.BytesSize),
		}

		if err := streamCacheEnd.Send(cacheRes); err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		streamCacheEnd.CloseSend()

		_, err = streamCacheEnd.Recv()
		if err != nil {
			return (helpers.AssertActual(assert.NoError, err, err.Error()))
		}
	}

	return nil
}

func (st *pluginTest) now(microseconds int) error {
	st.mt.microseconds = microseconds
	return nil
}

func (st *pluginTest) get(url string) error {
	req, err := http.NewRequest("GET", "/"+url, nil)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, "error creating request: %s", err.Error())
	}

	rec := httptest.NewRecorder()

	switch url {
	case "response-times":
		st.controller.GetResponseTime(rec, req)
	case "average-access-times":
		st.controller.GetAverageAccessTime(rec, req)
	case "average-error-times":
		st.controller.GetAverageErrorTime(rec, req)
	case "average-cache-size":
		st.controller.GetAverageCacheSize(rec, req)
	}

	st.result = rec
	st.results[url] = rec
	return nil
}

func (st *pluginTest) itShouldMatchJson(result string, expected *godog.DocString) error {
	rec := st.results[result].(*httptest.ResponseRecorder)
	return helpers.AssertExpectedActualJson(rec.Body.String(), expected.Content)
}
