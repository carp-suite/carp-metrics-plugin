Feature: Metrics Collection Plugin
  The plugin listen the start and end events, and it gathers data

  Scenario: Get response time
    Given the next requests: 
      | Id                                   | 
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 
    And the next responses: 
      | Id                                   | 
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 
    When the request start in the 2 microsecond 
    And the request end in the 5 microsecond
    Then I should get a response time of 3 microseconds

  Scenario: Get average access time of the route
    Given the next requests:
      | Id                                   | Path | Method |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | /app | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | /app | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | /opp | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de165 | /app | post   |
    When the request start in the 0 microsecond
    Then the plugin has a access time of:
      | Path   | /app |
      | Method | get |
    And this should access 2 times
    And this should has a average access time of 1 access/s in the 2 microsecond

  Scenario: Get average time between errors
    Given the next responses:
      | Id                                   | Status |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 500    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | 501    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | 599    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de165 | 499    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de166 | 600    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de167 | 550    |
    When the request end in the 0 microsecond
    Then the plugin should has 4 errors
    And this should has a error time of 2 microseconds in the 2 microsecond

  Scenario: Get average size of cache
    Given a cache with size 100 bytes
    And a cache with size 150 bytes
    When the cache end
    Then the plugin should has 125 bytes as average size of cache
