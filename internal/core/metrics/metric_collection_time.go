package metrics

import "time"

type MetricCollectionTime struct{}

func (mct MetricCollectionTime) Now() time.Time {
	return time.Now()
}

func (mct MetricCollectionTime) Since(t time.Time) time.Duration {
	return time.Since(t)
}
