package metrics

import "github.com/google/uuid"

type AverageCacheSize struct {
	Sizes map[uuid.UUID]float64
	Total float64
}

// Calculate average size of cache
func (s *AverageCacheSize) Calculate() {
	sum := float64(0)
	for _, size := range s.Sizes {
		sum += size
	}
	s.Total = sum / float64(len(s.Sizes))
}
