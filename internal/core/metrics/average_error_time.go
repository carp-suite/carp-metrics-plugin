package metrics

import (
	"time"
)

type ErrorTime struct {
	Mt    MetricTime
	Errs  int
	Total float64
}

// Calculate average time between errors of the route
func (t *ErrorTime) Calculate(pluginStart time.Time) {
	microseconds := float64(t.Mt.Since(pluginStart).Microseconds())
	t.Total = float64(t.Errs) / microseconds
}
