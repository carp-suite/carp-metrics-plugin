package metrics

import (
	"time"

	"gitlab.com/carp-suite/plugins/carpdriver"
)

type AccessTimeId struct {
	Path   string
	Method carpdriver.Method
}

type AccessTime struct {
	Mt     MetricTime
	Id     AccessTimeId
	Path   string
	Method carpdriver.Method
	Access int
	Total  float64
	Last   time.Time
}

// Calculate average access time of the route and cache
func (t *AccessTime) Calculate(pluginStart time.Time) {
	microseconds := float64(t.Mt.Since(pluginStart).Microseconds())
	t.Total = float64(t.Access) / microseconds
}
