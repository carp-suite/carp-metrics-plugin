package metrics

import (
	"time"

	"github.com/google/uuid"
)

type ResponseTime struct {
	Id    uuid.UUID
	Start time.Time
	End   time.Time
	Total time.Duration
}

// Calculate response time of the route
func (t *ResponseTime) Calculate() {
	t.Total = t.End.Sub(t.Start)
}
