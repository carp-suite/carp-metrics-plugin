package metrics

import "time"

type MetricTime interface {
	Now() time.Time
	Since(t time.Time) time.Duration
}
