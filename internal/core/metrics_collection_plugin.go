package core

import (
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core/metrics"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type MetricsCollectionPlugin struct {
	Mt            metrics.MetricTime
	Start         time.Time
	ResponseTimes map[uuid.UUID]*metrics.ResponseTime
	AccessTimes   map[metrics.AccessTimeId]*metrics.AccessTime
	ErrorTimes    metrics.ErrorTime
	CacheSizes    metrics.AverageCacheSize
}

func NewMetricsCollectionPlugin(mt metrics.MetricTime) *MetricsCollectionPlugin {
	p := &MetricsCollectionPlugin{
		Mt:            mt,
		Start:         mt.Now(),
		ResponseTimes: make(map[uuid.UUID]*metrics.ResponseTime),
		AccessTimes:   make(map[metrics.AccessTimeId]*metrics.AccessTime),
	}

	p.ErrorTimes.Mt = mt
	p.CacheSizes.Sizes = make(map[uuid.UUID]float64)

	return p
}

func (p *MetricsCollectionPlugin) Configure(ctx []byte) error {
	return nil
}

func (p *MetricsCollectionPlugin) RequestStart(req *carpdriver.Request) (*carpdriver.Request, *carpdriver.Response, error) {
	log.Println("Request received: ", req)

	// Response Time
	if p.ResponseTimes[req.ID] == nil {
		p.ResponseTimes[req.ID] = new(metrics.ResponseTime)
		p.ResponseTimes[req.ID].Id = req.ID
		p.ResponseTimes[req.ID].Start = p.Mt.Now()
	}

	// Access Time
	at := metrics.AccessTimeId{
		Path:   req.Path,
		Method: req.Method,
	}

	if p.AccessTimes[at] == nil {
		p.AccessTimes[at] = new(metrics.AccessTime)
		p.AccessTimes[at].Id = at
		p.AccessTimes[at].Path = at.Path
		p.AccessTimes[at].Method = at.Method
		p.AccessTimes[at].Mt = p.Mt
	}

	p.AccessTimes[at].Access++
	p.AccessTimes[at].Last = p.Mt.Now()
	p.AccessTimes[at].Calculate(p.Start)

	return nil, nil, nil
}

func (p *MetricsCollectionPlugin) RequestEnd(res *carpdriver.Response) (*carpdriver.Response, error) {
	// Response Time
	if p.ResponseTimes[res.ID] != nil {
		p.ResponseTimes[res.ID].End = p.Mt.Now()
		p.ResponseTimes[res.ID].Calculate()
	}

	// Errors
	if res.Status >= 500 && res.Status < 600 {
		p.ErrorTimes.Errs++
		p.ErrorTimes.Calculate(p.Start)
	}

	return nil, nil
}

func (p *MetricsCollectionPlugin) CacheStart(c *carpdriver.Cache) (*carpdriver.Cache, error) {
	return nil, nil
}

func (p *MetricsCollectionPlugin) CacheEnd(c *carpdriver.Cache) error {
	// Average size of responses on cache
	p.CacheSizes.Sizes[c.ID] = c.Size
	p.CacheSizes.Calculate()

	return nil
}
