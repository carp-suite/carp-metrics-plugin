package core_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core/metrics"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/helpers"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type metricsCollectionPluginTest struct {
	mt        *mockMetricTime
	plugin    *core.MetricsCollectionPlugin
	requests  []*carpdriver.Request
	responses []*carpdriver.Response
	caches    []*carpdriver.Cache
	result    interface{}
}

var methods map[string]int16 = map[string]int16{
	"get":     1,
	"post":    2,
	"patch":   3,
	"put":     4,
	"delete":  5,
	"options": 6,
}

type mockMetricTime struct {
	minutes      int
	microseconds int
}

func (mct mockMetricTime) Now() time.Time {
	return time.Date(0, 0, 0, 0, 0, mct.minutes, mct.microseconds*1000, time.UTC)
}

// TODO update with pt.mt.Now().Sub(t)
func (mct mockMetricTime) Since(t time.Time) time.Duration {
	duration, _ := time.ParseDuration(fmt.Sprint(mct.microseconds, "us"))
	return duration
}

func TestMetricsCollectionPlugin(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./metrics_collection_plugin.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	pt := new(metricsCollectionPluginTest)

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := pt.setup(sc)
		return ctx, err
	})

	sc.Step(`^the next (\w+):$`, pt.setNext)
	sc.Step(`^a cache with size (\d+) bytes$`, pt.setCache)
	sc.Step(`^the request start in the (\d+) microsecond$`, pt.requestStart)
	sc.Step(`^the request end in the (\d+) microsecond$`, pt.requestEnd)
	sc.Step(`^the cache end$`, pt.cacheEnd)
	sc.Step(`^I should get a response time of (\d+) microseconds$`, pt.shouldGetResponseTime)
	sc.Step(`^the plugin has a access time of:$`, pt.getAccessTime)
	sc.Step(`^this should access (\d+) times$`, pt.shouldAccess)
	sc.Step(`^this should has a access time of (\d+) microseconds in the (\d+) microsecond$`, pt.shouldHasAccessTimeOf)
	sc.Step(`^this should has a average access time of (\d+) access/s in the (\d+) microsecond$`, pt.shouldHasAccessTimeOf)
	sc.Step(`^the plugin should has (\d+) errors$`, pt.shouldHasErrors)
	sc.Step(`^this should has a error time of (\d+) microseconds in the (\d+) microsecond$`, pt.shouldHasErrorTimeOf)
	sc.Step(`^the plugin should has (\d+) bytes as average size of cache$`, pt.shouldHasAverageSizeOfCache)
}

func (pt *metricsCollectionPluginTest) setup(*godog.Scenario) error {
	pt.mt = new(mockMetricTime)
	pt.plugin = core.NewMetricsCollectionPlugin(pt.mt)
	pt.requests = make([]*carpdriver.Request, 0)
	pt.responses = make([]*carpdriver.Response, 0)
	pt.caches = make([]*carpdriver.Cache, 0)
	return nil
}

func (pt *metricsCollectionPluginTest) setNext(reqOrRes string, table *godog.Table) error {
	assist := assistdog.NewDefault()

	if reqOrRes == "requests" {
		type request struct {
			Id     string
			Path   string
			Method string
		}

		result, err := assist.CreateSlice(new(request), table)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		requests := result.([]*request)

		for _, r := range requests {
			request := new(carpdriver.Request)

			id, err := uuid.Parse(r.Id)
			if err != nil {
				return helpers.AssertActual(assert.NoError, err, err.Error())
			}
			request.ID = id

			request.Path = r.Path

			if r.Method != "" {
				request.Method = carpdriver.Method(methods[r.Method])
			}

			pt.requests = append(pt.requests, request)
		}

		return nil
	}

	if reqOrRes == "responses" {
		type response struct {
			Id     string
			Status int
		}

		result, err := assist.CreateSlice(new(response), table)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		responses := result.([]*response)

		for _, r := range responses {
			response := new(carpdriver.Response)

			id, err := uuid.Parse(r.Id)
			if err != nil {
				return helpers.AssertActual(assert.NoError, err, err.Error())
			}

			response.ID = id
			response.Status = r.Status

			pt.responses = append(pt.responses, response)
		}

		return nil
	}

	return nil
}

func (pt *metricsCollectionPluginTest) setCache(size int) error {
	pt.caches = append(pt.caches, &carpdriver.Cache{
		ID:   uuid.New(),
		Size: float64(size),
	})

	return nil
}

func (pt *metricsCollectionPluginTest) requestStart(startTime int) error {
	pt.mt.microseconds = startTime
	for _, r := range pt.requests {
		_, _, err := pt.plugin.RequestStart(r)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}
	}

	return nil
}

func (pt *metricsCollectionPluginTest) requestEnd(endTime int) error {
	pt.mt.microseconds = endTime
	for _, r := range pt.responses {
		_, err := pt.plugin.RequestEnd(r)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}
	}

	return nil
}

func (pt *metricsCollectionPluginTest) cacheEnd() error {
	for _, c := range pt.caches {
		err := pt.plugin.CacheEnd(c)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}
	}

	return nil
}

func (pt *metricsCollectionPluginTest) shouldGetResponseTime(microseconds int) error {
	id := pt.requests[0].ID
	total := pt.plugin.ResponseTimes[id].Total

	err := helpers.AssertExpectedActual(
		assert.Equal, int64(microseconds), total.Microseconds(),
		"Expected %v microseconds, but there is %v", int64(microseconds), total.Microseconds(),
	)
	if err != nil {
		return err
	}

	return nil
}

func (pt *metricsCollectionPluginTest) getAccessTime(accessTimeId *godog.Table) error {
	assist := assistdog.NewDefault()
	result, err := assist.ParseMap(accessTimeId)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	atid := metrics.AccessTimeId{
		Path:   result["Path"],
		Method: carpdriver.Method(methods[result["Method"]]),
	}

	pt.result = pt.plugin.AccessTimes[atid]
	return nil
}

func (pt *metricsCollectionPluginTest) shouldAccess(times int) error {
	actual := pt.result.(*metrics.AccessTime)

	err := helpers.AssertExpectedActual(
		assert.Equal, times, actual.Access,
		"Expected %v times, but there is %v", times, actual.Access,
	)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	return nil
}

func (pt *metricsCollectionPluginTest) shouldHasAccessTimeOf(accessTime int, now int) error {
	actual := pt.result.(*metrics.AccessTime)
	pt.mt.microseconds = now
	actual.Calculate(pt.mt.Now())
	total := actual.Total

	err := helpers.AssertExpectedActual(
		assert.Equal, float64(accessTime), total,
		"Expected %v microseconds, but there is %v microseconds", float64(accessTime), total,
	)
	if err != nil {
		return err
	}

	return nil
}

func (pt *metricsCollectionPluginTest) shouldHasErrors(errs int) error {
	actual := pt.plugin.ErrorTimes

	err := helpers.AssertExpectedActual(
		assert.Equal, errs, actual.Errs,
		"Expected %v times, but there is %v", errs, actual.Errs,
	)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	return nil
}

func (pt *metricsCollectionPluginTest) shouldHasErrorTimeOf(errorTime int, now int) error {
	actual := pt.plugin.ErrorTimes
	pt.mt.microseconds = now
	actual.Calculate(pt.mt.Now())
	total := actual.Total

	err := helpers.AssertExpectedActual(
		assert.Equal, float64(errorTime), total,
		"Expected %v microseconds, but there is %v microseconds", float64(errorTime), total,
	)
	if err != nil {
		return err
	}

	return nil
}

func (pt *metricsCollectionPluginTest) shouldHasAverageSizeOfCache(expected int) error {
	actual := pt.plugin.CacheSizes
	actual.Calculate()
	total := actual.Total

	err := helpers.AssertExpectedActual(
		assert.Equal, float64(expected), total,
		"Expected %v bytes, but there is %v bytes", float64(expected), total,
	)
	if err != nil {
		return err
	}

	return nil
}
