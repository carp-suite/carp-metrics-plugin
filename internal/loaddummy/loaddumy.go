package loaddummy

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core/metrics"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

func LoadDummyData(plugin *core.MetricsCollectionPlugin) error {
	// Load response times

	for i := 0; i < 15; i++ {
		id := uuid.New()
		plugin.ResponseTimes[id] = &metrics.ResponseTime{
			Id:    id,
			Start: time.Date(0, 0, 0, 0, 0, 0, i*1000, time.UTC),
			End:   time.Date(0, 0, 0, 0, 0, 0, i*4*1000, time.UTC),
		}
		plugin.ResponseTimes[id].Calculate()
	}

	for i := 0; i < 15; i++ {
		id := uuid.New()
		plugin.ResponseTimes[id] = &metrics.ResponseTime{
			Id:    id,
			Start: time.Date(0, 0, 0, 0, 0, 0, i*5*1000, time.UTC),
			End:   time.Date(0, 0, 0, 0, 0, 0, i*6*1000, time.UTC),
		}
		plugin.ResponseTimes[id].Calculate()
	}

	// Load access times
	accessTimeIds := []metrics.AccessTimeId{
		{Path: "/api/v1/health", Method: carpdriver.Method(1)},
		{Path: "/api/v1/health", Method: carpdriver.Method(2)},
		{Path: "/api/v1/health", Method: carpdriver.Method(3)},
		{Path: "/api/v1/health", Method: carpdriver.Method(4)},
		{Path: "/api/v1/health", Method: carpdriver.Method(5)},
		{Path: "/api/v1/health", Method: carpdriver.Method(6)},
		{Path: "/api/v2/health", Method: carpdriver.Method(1)},
		{Path: "/api/v3/health", Method: carpdriver.Method(1)},
		{Path: "/api/v4/health", Method: carpdriver.Method(1)},
	}

	for i, id := range accessTimeIds {
		plugin.AccessTimes[id] = &metrics.AccessTime{
			Mt:     plugin.Mt,
			Path:   id.Path,
			Method: id.Method,
			Id:     id,
			Access: 500 + int(id.Method),
			Last:   time.Date(0, 0, 0, 0, 0, 0, i*1000, time.UTC),
		}
	}

	// Load errors
	plugin.ErrorTimes.Errs = 500

	// Load cache sizes
	plugin.CacheSizes.Sizes = []float64{100, 200, 300, 400, 500}
	plugin.CacheSizes.Calculate()

	return nil
}
