Feature: Metrics Collection Plugin Service

  Scenario: Get metrics collection
    Given the request start and end:
      | Id                                   | Path | Method | Start | Status | End |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | /app | get    | 2     | 200    | 4   |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | /app | get    | 3     | 500    | 9   |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | /opp | get    | 4     | 501    | 16  |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de165 | /app | post   | 5     | 404    | 25  |
    And the cache end:
      | Id                                   | BytesSize |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 100       |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | 150       |
    When now is the 40 microsecond
    And I get response-times
    And I get average-access-times
    And I get average-error-times
    And I get average-cache-size
    Then the response-times should match json
    """
    [
      "<<UNORDERED>>",
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de162",
        "responseTime": "2 µs",
        "finishedAt": "Nov 30 00:00:00.000004"
      },
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de163",
        "responseTime": "6 µs",
        "finishedAt": "Nov 30 00:00:00.000009"
      },
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de164",
        "responseTime": "12 µs",
        "finishedAt": "Nov 30 00:00:00.000016"
      },
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de165",
        "responseTime": "20 µs",
        "finishedAt": "Nov 30 00:00:00.000025"
      }
    ]
    """
    And the average-access-times should match json
    """
    [
      "<<UNORDERED>>",
      {
        "path": "/app",
        "method": "get",
        "averageAccessTime": "0.05 access/µs"
      },
      {
        "path": "/app",
        "method": "post",
        "averageAccessTime": "0.025 access/µs"
      },
      {
        "path": "/opp",
        "method": "get",
        "averageAccessTime": "0.025 access/µs"
      }
    ]
    """
    And the average-error-times should match json
    """
    {
      "averageErrorTime": "0.05 errors/µs"
    }
    """
    And the average-cache-size should match json
    """
    {
      "averageCacheSize": "125 bytes"
    }
    """
