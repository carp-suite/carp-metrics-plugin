package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"time"

	"github.com/google/uuid"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core/metrics"
	"golang.org/x/net/websocket"
)

type MetricsCollectionPluginController struct {
	Plugin *core.MetricsCollectionPlugin
}

func (c *MetricsCollectionPluginController) SendResponseTime(ws *websocket.Conn) {
	type responseTime struct {
		Id         uuid.UUID `json:"id"`
		Total      string    `json:"responseTime"`
		FinishedAt string    `json:"finishedAt"`
	}

	var err error
	var respJson []byte

	for {
		responseTimesSorted := make([]*metrics.ResponseTime, 0)
		for _, rt := range c.Plugin.ResponseTimes {
			responseTimesSorted = append(responseTimesSorted, rt)
		}
		sort.Slice(responseTimesSorted, func(i, j int) bool {
			return responseTimesSorted[i].End.After(responseTimesSorted[j].End)
		})

		resp := make([]responseTime, 0)
		for _, rt := range responseTimesSorted {
			resp = append(resp, responseTime{
				Id:         rt.Id,
				Total:      fmt.Sprintf("%d µs", rt.Total.Nanoseconds()/1000),
				FinishedAt: rt.End.Format(time.StampMicro),
			})
		}

		respJson, err = json.Marshal(resp)
		if err != nil {
			fmt.Println(err)
		}

		if err = websocket.Message.Send(ws, string(respJson)); err != nil {
			fmt.Println("Can't send because ", err)
		}

		time.Sleep(1 * time.Second)
	}
}

func (c *MetricsCollectionPluginController) SendAverageAccessTime(ws *websocket.Conn) {
	type accessTime struct {
		Path   string `json:"path"`
		Method string `json:"method"`
		Total  string `json:"averageAccessTime"`
	}

	var err error
	var respJson []byte

	for {
		accessTimesSorted := make([]*metrics.AccessTime, 0)
		for _, at := range c.Plugin.AccessTimes {
			accessTimesSorted = append(accessTimesSorted, at)
		}
		sort.Slice(accessTimesSorted, func(i, j int) bool {
			return accessTimesSorted[i].Last.After(accessTimesSorted[j].Last)
		})

		resp := make([]accessTime, 0)
		for _, at := range accessTimesSorted {
			at.Calculate(c.Plugin.Start)
			resp = append(resp, accessTime{
				Path:   at.Id.Path,
				Method: at.Id.Method.String(),
				Total:  fmt.Sprintf("%v access/µs", at.Total),
			})
		}

		respJson, err = json.Marshal(resp)
		if err != nil {
			fmt.Println(err)
			break
		}

		if err = websocket.Message.Send(ws, string(respJson)); err != nil {
			fmt.Println("Can't send because ", err)
			break
		}

		time.Sleep(1 * time.Second)
	}
}

func (c *MetricsCollectionPluginController) SendAverageErrorTime(ws *websocket.Conn) {
	type errorTime struct {
		Total string `json:"averageErrorTime"`
	}

	var err error
	var resp errorTime = errorTime{}
	var respJson []byte

	for {
		c.Plugin.ErrorTimes.Calculate(c.Plugin.Start)
		resp.Total = fmt.Sprintf("%v errors/µs", c.Plugin.ErrorTimes.Total)

		respJson, err = json.Marshal(resp)
		if err != nil {
			fmt.Println(err)
			break
		}

		if err = websocket.Message.Send(ws, string(respJson)); err != nil {
			fmt.Println("Can't send because ", err)
			break
		}

		time.Sleep(1 * time.Second)
	}
}

func (c *MetricsCollectionPluginController) SendAverageCacheSize(ws *websocket.Conn) {
	type cacheSize struct {
		Total string `json:"averageCacheSize"`
	}

	var err error
	var resp cacheSize = cacheSize{}
	var respJson []byte

	for {
		resp.Total = fmt.Sprintf("%v bytes", c.Plugin.CacheSizes.Total)
		respJson, err = json.Marshal(resp)
		if err != nil {
			fmt.Println(err)
		}

		if err = websocket.Message.Send(ws, string(respJson)); err != nil {
			fmt.Println("Can't send because ", err)
		}
		time.Sleep(1 * time.Second)
	}
}

func (c *MetricsCollectionPluginController) GetResponseTime(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported.", http.StatusMethodNotAllowed)
		return
	}

	(w).Header().Set("Access-Control-Allow-Origin", "*")

	type responseTime struct {
		Id         uuid.UUID `json:"id"`
		Total      string    `json:"responseTime"`
		FinishedAt string    `json:"finishedAt"`
	}

	responseTimesSorted := make([]*metrics.ResponseTime, 0)
	for _, rt := range c.Plugin.ResponseTimes {
		responseTimesSorted = append(responseTimesSorted, rt)
	}
	sort.Slice(responseTimesSorted, func(i, j int) bool {
		return responseTimesSorted[i].End.After(responseTimesSorted[j].End)
	})

	resp := make([]responseTime, 0)
	for _, rt := range responseTimesSorted {
		resp = append(resp, responseTime{
			Id:         rt.Id,
			Total:      fmt.Sprintf("%d µs", rt.Total.Nanoseconds()/1000),
			FinishedAt: rt.End.Format(time.StampMicro),
		})
	}

	respJson, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(respJson)
}

func (c *MetricsCollectionPluginController) GetAverageAccessTime(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported.", http.StatusMethodNotAllowed)
		return
	}

	(w).Header().Set("Access-Control-Allow-Origin", "*")

	type accessTime struct {
		Path   string `json:"path"`
		Method string `json:"method"`
		Total  string `json:"averageAccessTime"`
	}

	accessTimesSorted := make([]*metrics.AccessTime, 0)
	for _, at := range c.Plugin.AccessTimes {
		accessTimesSorted = append(accessTimesSorted, at)
	}
	sort.Slice(accessTimesSorted, func(i, j int) bool {
		return accessTimesSorted[i].Last.After(accessTimesSorted[j].Last)
	})

	resp := make([]accessTime, 0)
	for _, at := range accessTimesSorted {
		at.Calculate(c.Plugin.Start)
		resp = append(resp, accessTime{
			Path:   at.Id.Path,
			Method: at.Id.Method.String(),
			Total:  fmt.Sprintf("%v access/µs", at.Total),
		})
	}

	respJson, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(respJson)
}

func (c *MetricsCollectionPluginController) GetAverageErrorTime(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported.", http.StatusMethodNotAllowed)
		return
	}

	(w).Header().Set("Access-Control-Allow-Origin", "*")

	type errorTime struct {
		Total string `json:"averageErrorTime"`
	}

	c.Plugin.ErrorTimes.Calculate(c.Plugin.Start)
	resp := errorTime{
		Total: fmt.Sprintf("%v errors/µs", c.Plugin.ErrorTimes.Total),
	}

	respJson, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(respJson)
}

func (c *MetricsCollectionPluginController) GetAverageCacheSize(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method is not supported.", http.StatusMethodNotAllowed)
		return
	}

	(w).Header().Set("Access-Control-Allow-Origin", "*")

	type cacheSize struct {
		Total string `json:"averageCacheSize"`
	}

	resp := cacheSize{
		Total: fmt.Sprintf("%v bytes", c.Plugin.CacheSizes.Total),
	}

	respJson, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(respJson)
}
