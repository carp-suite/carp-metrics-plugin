package server_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/cucumber/godog"
	"github.com/google/uuid"
	"github.com/rdumont/assistdog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/helpers"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/server"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

type metricsCollectionPluginControllerTest struct {
	mt         *mockMetricTime
	plugin     *core.MetricsCollectionPlugin
	controller *server.MetricsCollectionPluginController
	result     interface{}
}

var methods map[string]int16 = map[string]int16{
	"get":     1,
	"post":    2,
	"patch":   3,
	"put":     4,
	"delete":  5,
	"options": 6,
}

type mockMetricTime struct {
	microseconds int
}

func (mct mockMetricTime) Now() time.Time {
	t := time.Date(0, 0, 0, 0, 0, 0, mct.microseconds*1000, time.UTC)
	return t
}

func (mct mockMetricTime) Since(t time.Time) time.Duration {
	return mct.Now().Sub(t)
}

func TestMetricsCollectionPluginController(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"./metrics_collection_plugin_controller.feature"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run plugin tests")
	}
}

func InitializeScenario(sc *godog.ScenarioContext) {
	ct := new(metricsCollectionPluginControllerTest)

	sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		err := ct.setup(sc)
		return ctx, err
	})

	sc.Step(`^the request (\w+) in the microseconds:$`, ct.requestInMicroseconds)
	sc.Step(`^the request (\w+) in the (\d+) microsecond:$`, ct.requestInMicrosecond)
	sc.Step(`^the cache end:$`, ct.cacheEnd)
	sc.Step(`^I get (.+) in the microsecond (\d+)$`, ct.get)
	sc.Step(`^it should be status (\d+)$`, ct.itShouldBeStatus)
	sc.Step(`^it should match json$`, ct.itShouldMatchJson)
}

func (ct *metricsCollectionPluginControllerTest) setup(sc *godog.Scenario) error {
	ct.mt = new(mockMetricTime)
	ct.plugin = core.NewMetricsCollectionPlugin(ct.mt)
	ct.controller = &server.MetricsCollectionPluginController{
		Plugin: ct.plugin,
	}

	return nil
}

func (ct *metricsCollectionPluginControllerTest) requestInMicroseconds(action string, requests *godog.Table) error {
	type request struct {
		Id    string
		Start int
		End   int
	}

	assist := assistdog.NewDefault()
	result, err := assist.CreateSlice(new(request), requests)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	results := result.([]*request)

	if action == "start" {
		for _, r := range results {

			id, err := uuid.Parse(r.Id)
			if err != nil {
				return helpers.AssertActual(assert.NoError, err, err.Error())
			}

			req := &carpdriver.Request{
				ID: id,
			}

			ct.mt.microseconds = r.Start
			ct.plugin.RequestStart(req)
		}

		return nil
	}

	for _, r := range results {
		id, err := uuid.Parse(r.Id)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		res := &carpdriver.Response{
			ID: id,
		}

		ct.mt.microseconds = r.End
		ct.plugin.RequestEnd(res)
	}

	return nil
}

func (ct *metricsCollectionPluginControllerTest) requestInMicrosecond(action string, microsecond int, table *godog.Table) error {
	ct.mt.microseconds = microsecond

	if action == "start" {
		type request struct {
			Id     string
			Path   string
			Method string
		}

		assist := assistdog.NewDefault()
		result, err := assist.CreateSlice(new(request), table)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		requests := result.([]*request)
		for _, r := range requests {

			id, err := uuid.Parse(r.Id)
			if err != nil {
				return helpers.AssertActual(assert.NoError, err, err.Error())
			}

			req := &carpdriver.Request{
				ID:     id,
				Method: carpdriver.Method(methods[r.Method]),
				Path:   r.Path,
			}

			ct.plugin.RequestStart(req)
		}

		return nil
	}

	if action == "end" {
		type response struct {
			Id     string
			Status int
		}

		assist := assistdog.NewDefault()
		result, err := assist.CreateSlice(new(response), table)
		if err != nil {
			return helpers.AssertActual(assert.NoError, err, err.Error())
		}

		responses := result.([]*response)
		for _, r := range responses {

			id, err := uuid.Parse(r.Id)
			if err != nil {
				return helpers.AssertActual(assert.NoError, err, err.Error())
			}

			res := &carpdriver.Response{
				ID:     id,
				Status: r.Status,
			}

			ct.plugin.RequestEnd(res)
		}

		return nil
	}

	return nil
}

func (ct *metricsCollectionPluginControllerTest) cacheEnd(table *godog.Table) error {
	type cache struct {
		BytesSize int
	}

	assist := assistdog.NewDefault()
	result, err := assist.CreateSlice(new(cache), table)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, err.Error())
	}

	caches := result.([]*cache)
	for _, c := range caches {
		cache := &carpdriver.Cache{
			Size: float64(c.BytesSize),
		}
		ct.plugin.CacheEnd(cache)
	}

	return nil
}

func (ct *metricsCollectionPluginControllerTest) get(url string, microsecond int) error {
	ct.mt.microseconds = microsecond

	req, err := http.NewRequest("GET", "/"+url, nil)
	if err != nil {
		return helpers.AssertActual(assert.NoError, err, "error creating request: %s", err.Error())
	}

	rec := httptest.NewRecorder()

	if url == "response-times" {
		ct.controller.GetResponseTime(rec, req)
		ct.result = rec
		return nil
	}

	if url == "average-access-times" {
		ct.controller.GetAverageAccessTime(rec, req)
		ct.result = rec
		return nil
	}

	if url == "average-error-times" {
		ct.controller.GetAverageErrorTime(rec, req)
		ct.result = rec
		return nil
	}

	if url == "average-cache-size" {
		ct.controller.GetAverageCacheSize(rec, req)
		ct.result = rec
		return nil
	}

	return nil
}

func (ct *metricsCollectionPluginControllerTest) itShouldBeStatus(status int) error {
	rec := ct.result.(*httptest.ResponseRecorder)
	return helpers.AssertExpectedActual(
		assert.Equal, rec.Code, status,
		"Expected response status to be %d, but got %d", status, rec.Code,
	)
}

func (ct *metricsCollectionPluginControllerTest) itShouldMatchJson(expected *godog.DocString) error {
	rec := ct.result.(*httptest.ResponseRecorder)
	return helpers.AssertExpectedActualJson(rec.Body.String(), expected.Content)
}
