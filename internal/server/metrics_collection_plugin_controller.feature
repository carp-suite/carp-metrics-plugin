Feature: Metrics Collection Plugin Controller
  The controller expose endpoints by metrics collection plugin.

  Scenario: Get response times
    Given the request start in the microseconds:
      | Id                                   | Start |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 2     |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | 3     |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | 4     |
    And the request end in the microseconds:
      | Id                                   | End |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 4   |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | 9   |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | 16  |
    When I get response-times in the microsecond 16
    Then it should be status 200
    And it should match json
    """
    [
      "<<UNORDERED>>",
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de162",
        "responseTime": "2 µs",
        "finishedAt": "Nov 30 00:00:00.000004"
      },
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de163",
        "responseTime": "6 µs",
        "finishedAt": "Nov 30 00:00:00.000009"
      },
      {
        "id": "e38a9003-bc2d-4ba3-ac5d-d6a8c74de164",
        "responseTime": "12 µs",
        "finishedAt": "Nov 30 00:00:00.000016"
      }
    ]
    """

  Scenario: Get average access time
    Given the request start in the 0 microsecond:
      | Id                                   | Path | Method |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | /app | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | /app | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | /opp | get    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de165 | /app | post   |
    When I get average-access-times in the microsecond 2
    Then it should be status 200
    And it should match json
    """
    [
      "<<UNORDERED>>",
      {
        "path": "/app",
        "method": "get",
        "averageAccessTime": "1 access/µs"
      },
      {
        "path": "/app",
        "method": "post",
        "averageAccessTime": "0.5 access/µs"
      },
      {
        "path": "/opp",
        "method": "get",
        "averageAccessTime": "0.5 access/µs"
      }
    ]
    """

  Scenario: Get average time between errors
    Given the request end in the 0 microsecond:
      | Id                                   | Status |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de162 | 500    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de163 | 501    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de164 | 599    |
      | e38a9003-bc2d-4ba3-ac5d-d6a8c74de167 | 550    |
    When I get average-error-times in the microsecond 2
    Then it should be status 200
    And it should match json
    """
    {
      "averageErrorTime": "2 errors/µs"
    }
    """

  Scenario: Get average size of cache
    Given the cache end:
      | BytesSize |
      | 100       |
      | 150       |
    When I get average-cache-size in the microsecond 0
    Then it should be status 200
    And it should match json
    """
    {
      "averageCacheSize": "125 bytes"
    }
    """

