package server

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/net/websocket"
)

type MetricsCollectionPluginServer struct {
	Controller *MetricsCollectionPluginController
}

func Echo(ws *websocket.Conn) {
	var err error

	for {
		if err = websocket.Message.Send(ws, "Hello"); err != nil {
			fmt.Println("Can't send because ", err)
			break
		}

		time.Sleep(1 * time.Second)
	}
}

func (s *MetricsCollectionPluginServer) Serve(port string) {
	http.Handle("/ws-response-times", websocket.Handler(s.Controller.SendResponseTime))
	http.Handle("/ws-average-access-times", websocket.Handler(s.Controller.SendAverageAccessTime))
	http.Handle("/ws-average-error-times", websocket.Handler(s.Controller.SendAverageErrorTime))
	http.Handle("/ws-average-cache-size", websocket.Handler(s.Controller.SendAverageCacheSize))
	http.HandleFunc("/response-times", s.Controller.GetResponseTime)
	http.HandleFunc("/average-access-times", s.Controller.GetAverageAccessTime)
	http.HandleFunc("/average-error-times", s.Controller.GetAverageErrorTime)
	http.HandleFunc("/average-cache-size", s.Controller.GetAverageCacheSize)

	log.Fatal(http.ListenAndServe(port, nil))
}
