package main

import (
	"log"
	"net"
	"os"

	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/core/metrics"
	"gitlab.com/carp-suite/plugins/carpdriver"

	// "gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/loaddummy"
	"gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin/internal/server"
)

const (
	protocol = "unix"
	sockAddr = "/run/carp/cmc.sock"
)

func main() {
	mt := new(metrics.MetricCollectionTime)
	plugin := core.NewMetricsCollectionPlugin(mt)
	controller := &server.MetricsCollectionPluginController{Plugin: plugin}
	server := &server.MetricsCollectionPluginServer{Controller: controller}

	// loaddummy.LoadDummyData(plugin)

	go func() {
		// port := 8081
		// lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
		// if err != nil {
		// 	log.Fatalf("failed to listen: %v", err)
		// }

		if _, err := os.Stat(sockAddr); !os.IsNotExist(err) {
			if err := os.RemoveAll(sockAddr); err != nil {
				log.Fatal(err)
			}
		}

		listener, err := net.Listen(protocol, sockAddr)
		if err != nil {
			log.Fatal(err)
		}

		gs := carpdriver.InitGrpc(plugin)

		log.Println("Server gRPC starting on ", sockAddr)
		if err := gs.Serve(listener); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	log.Println("Server starting on port 8082")
	server.Serve(":8082")
}
