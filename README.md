# CARP Metrics Collection Plugin

The cache and route hit metrics collection plugin will listen to the cache and route start and end events ([specified in the proto](https://gitlab.com/carp-suite/plugins/carpdriver/-/blob/main/carp.proto)) to provide the following metrics to a web server via websockets:

- Route response time.
- Average access time in one second of the route.
- Mean time between path failures.
- Average size of responses stored in a cache.

## 🚀 Get Started

 Go 1.19 is required

1. Clone the repository

2. Exec in the root:

```bash
go run ./cmd/main/main.go
```

## 🐞 Testing

```bash
go test ./.../
```
