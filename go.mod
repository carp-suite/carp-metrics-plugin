module gitlab.com/carp-suite/plugins/carp-metrics-collection-plugin

go 1.19

require (
	github.com/cucumber/godog v0.12.6
	github.com/google/uuid v1.3.1
	github.com/kinbiko/jsonassert v1.1.1
	github.com/rdumont/assistdog v0.0.0-20201106100018-168b06230d14
	github.com/stretchr/testify v1.8.2
	gitlab.com/carp-suite/plugins/carpdriver v0.0.0-20231014234207-8cfca40c2878
	golang.org/x/net v0.17.0
	google.golang.org/grpc v1.58.3
)

require (
	github.com/cucumber/gherkin-go/v19 v19.0.3 // indirect
	github.com/cucumber/messages-go/v16 v16.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/flatbuffers v23.5.26+incompatible // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/go-memdb v1.3.2 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/wapc/wapc-guest-tinygo v0.3.3 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231012201019-e917dd12ba7a // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
